package ca.qc.claurendeau.exception;

public class BufferNullPointerException extends Exception {

    public BufferNullPointerException() {
        super("Element is null");
    }
}
