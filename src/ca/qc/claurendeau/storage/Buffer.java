package ca.qc.claurendeau.storage;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import ca.qc.claurendeau.exception.BufferNullPointerException;

public class Buffer {
    private CircularFifoQueue<Element> filo;

    public Buffer(int capacity) {
        this.filo = new CircularFifoQueue<>(capacity);
    }
    
    public String toString() {
        return filo.toString();
    }

    public int capacity() {
        return filo.maxSize();
    }

    public int getCurrentLoad() {
        return filo.size();
    }

    public boolean isEmpty() {
        return filo.size() == 0;
    }

    public boolean isFull() {
        return filo.maxSize() == filo.size();
    }

    public synchronized void addElement(Element element) throws Exception {
        if (element == null) {
            throw new BufferNullPointerException();
        }
        if (isFull()) {
            throw new BufferFullException();
        }
        this.filo.add(element);
    }

    public synchronized Element removeElement() throws BufferEmptyException {
        if (isEmpty()) {
            throw new BufferEmptyException();
        }
        return filo.remove();
    }
}
