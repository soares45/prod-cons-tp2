package ca.qc.claurendeau.storage;

public class Element
{
    private int data;

    public void setData(int data)
    {
        this.data = data;
    }
    
    public int getData()
    {
        return data;
    }
}
