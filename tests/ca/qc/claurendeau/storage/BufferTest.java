package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import ca.qc.claurendeau.exception.BufferNullPointerException;

public class BufferTest {

    final int SIZE_BUFFER = 2;
    Buffer buffer = null;
    
    @Before
    public void stepUp() {
        buffer = new Buffer(SIZE_BUFFER);
    }
    
    @After
    public void reset() {
        buffer = null;
    }
    
    @Test
    public void testNormalAddElement() throws Exception {
        Element element = new Element();
        for (int i = 0; i < SIZE_BUFFER; i++) {
            element.setData(i);
            buffer.addElement(element);
        }
        assertTrue(buffer.isFull());
    }
    
    @Test(expected = BufferFullException.class)
    public void testAnormalAddElement() throws Exception {
        Element element = new Element();
        for (int i = 0; i <= SIZE_BUFFER; i++) {
            element.setData(i);
            buffer.addElement(element);
        }
    }
    
    @Test(expected = BufferNullPointerException.class)
    public void testImposibleAddElement() throws Exception {
        buffer.addElement(null);
    }
    
    @Test
    public void testNormalCapacity() throws Exception {
        assertTrue(buffer.capacity() == SIZE_BUFFER);
    }
    
    @Test
    public void testFullIsFull() throws Exception {
        Element element = new Element();
        for (int i = 0; i < SIZE_BUFFER; i++) {
            element.setData(i);
            buffer.addElement(element);
        }
        assertTrue(buffer.isFull());
    }
    
    @Test
    public void testEmptyIsFull() throws Exception {
        assertFalse(buffer.isFull());
    }
    
    @Test
    public void testFullIsEmpty() throws Exception {
        Element element = new Element();
        for (int i = 0; i < SIZE_BUFFER; i++) {
            element.setData(i);
            buffer.addElement(element);
        }
        assertFalse(buffer.isEmpty());
    }
    
    @Test
    public void testEmptyIsEmpty() throws Exception {
        assertTrue(buffer.isEmpty());
    }
    
    @Test
    public void testFullGetCurrentLoad() throws Exception {
        Element element = new Element();
        for (int i = 0; i < SIZE_BUFFER; i++) {
            element.setData(i);
            buffer.addElement(element);
        }
        assertTrue(buffer.getCurrentLoad() == SIZE_BUFFER);
    }
    
    @Test
    public void testEmptyGetCurrentLoad() throws Exception {
        assertTrue(buffer.getCurrentLoad() == 0);
    }
    
    @Test
    public void testNormalRemoveElement() throws Exception {
        Element element = new Element();
        for (int i = 0; i < SIZE_BUFFER; i++) {
            element.setData(i);
            buffer.addElement(element);
        }
        assertEquals(element, buffer.removeElement());
    }
    
    @Test(expected = BufferEmptyException.class)
    public void testAnormalRemoveElement() throws Exception {
        assertEquals(new Element(), buffer.removeElement());
    }
    
    @Test
    public void testToString() throws Exception {
        Element element = new Element();
        for (int i = 0; i < SIZE_BUFFER; i++) {
            element.setData(i);
            buffer.addElement(element);
        }
        assertNotNull(buffer.toString());
    }

}


